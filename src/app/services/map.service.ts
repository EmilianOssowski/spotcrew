import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {VenueModel} from '../models/venue.model';

@Injectable({
  providedIn: 'root'
})
export class MapService {
  venues: BehaviorSubject<Array<VenueModel>> = new BehaviorSubject<Array<VenueModel>>([]);
  position: BehaviorSubject<any> = new BehaviorSubject(null);
  constructor() {  }
  // to do - pobieranie listy obiektów dla danej lokalizacji
  getVenues() {
    this.venues.next([
      new VenueModel(1, 'PWL', 'Plac Wiosny Ludów', 52.40554764996304, 16.930092573165897),
      new VenueModel(2, 'KwA', 'Kolegium w Antykwariacie', 52.40802825814948, 16.928215026855472),
      new VenueModel(3, 'POA', 'Politechnika Open Air', 52.41117626979609, 16.91630601882935)]);
  }
  // określenie lokalizacji uzytkownika
  findMe() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: Position) => {
        this.position.next(position);
      });
    } else {
      alert('Geolocation is not supported by this browser.');
    }
  }
}
