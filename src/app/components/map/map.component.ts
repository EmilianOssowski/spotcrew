import { Component, OnInit } from '@angular/core';
import {latLng, marker, tileLayer} from 'leaflet';
import * as L from 'leaflet';
import {VenueModel} from '../../models/venue.model';
import {MapService} from '../../services/map.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  venues: Array<VenueModel> = [];
  options = {
    layers: [
      tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; OpenStreetMap contributors'
      }),
      // tileLayer('https://2.base.maps.api.here.com/maptile/2.1/maptile/newest/reduced.day/{z}/{x}/{y}/512/png8?app_id=' +
      // 'IkVG1gMYxHv0fDCrrxDl' + '&app_code=' + 'G0EaPB8vFmovxk__QLZXXw' + '&ppi=320'
      // )

    ],
    zoom: 7,
    center: latLng([52, 19])
  };
  layers = [];  // tablica ze znacznikami
  map;  // obiekt reprezentujący mape, dzięki czemu można działać na mape z poziomu typescript

  markerIconUser = L.icon({
    iconUrl: './assets/icons/usermarker.png',

    iconSize:     [32, 32], // size of the icon
    iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
    popupAnchor:  [16, 0] // point from which the popup should open relative to the iconAnchor
  });
  markerIconCourt = L.icon({
    iconUrl: './assets/icons/boisko32.png',

    iconSize:     [32, 32], // size of the icon
    iconAnchor:   [0, 0], // point of the icon which will correspond to marker's location
    popupAnchor:  [16, 0] // point from which the popup should open relative to the iconAnchor
  });

  constructor(private mapService: MapService) { }
  ngOnInit() {
    this.mapService.venues.subscribe((res: Array<VenueModel>) => {
      this.venues = res;
      this.venues.forEach(venue => {
        this.addMarker(venue.latitude, venue.longitude, venue.name, venue.description, this.markerIconCourt);
      });
    });
    this.mapService.position.subscribe((pos: Position) => {
      if (pos != null) {
        this.markMe(pos.coords.latitude, pos.coords.longitude);
      }
    });
    this.mapService.findMe();
    this.mapService.getVenues();
  }
  // zaznaczenie znacznika na mapie reprezentującego uzytkownika
  markMe(lat, lng) {
    this.centerMap(lat, lng);
    this.addMarker(lat, lng, 'Twoja lokalizacja', '',  this.markerIconUser);
  }
  // odświeżanie lokalizacji
  refreshLocalization() {
    this.mapService.findMe();
  }
  // event po załadowaniu mapy
  onMapReady(map: L.Map) {
    this.map = map;
    this.mapService.findMe();
  }
  // do debugowania
  showCoordinates(event) {
    console.log(event.latlng);
  }


  // wyśrodkowanie mapy w danym punkcie
  centerMap(lat, lng) {
    this.map.setView(latLng(lat, lng), 14);
  }

  // dodawanie znacznika do mapy
  addMarker(lat, lng, name, description = null, markerIcon) {
    this.layers.push(marker([lat, lng]).bindPopup('<b>' + name + '</b><br>' + description + '<br>' + '<button></button>').setIcon(
    this.markerIconCourt));
  }
  // to do - pobieranie listy venues
  findVenues() {
    console.log(this.map.getCenter());
  }

}
